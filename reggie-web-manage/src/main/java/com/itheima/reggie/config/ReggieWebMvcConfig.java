package com.itheima.reggie.config;

import com.itheima.reggie.common.JacksonObjectMapper;
import com.itheima.reggie.interceptor.LoginCheckinterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;
//web相关配置

@Configuration
public class ReggieWebMvcConfig  implements WebMvcConfigurer {
    //设置静态资源

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        //文档需要映射的静态资源
        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    //拦截器
    @Autowired
    private LoginCheckinterceptor loginCheckinterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String>arrayList=new ArrayList<>();
       arrayList.add("/backend/**");
       arrayList.add("/error");
       arrayList.add("/employee/login");
       arrayList.add("/employee/logout");
        //文档需要释放的静态资源
        arrayList.add("/doc.html");
        arrayList.add("/webjars/**");
        arrayList.add("/swagger-resources");
        arrayList.add("/v2/api-docs");
       registry.addInterceptor(loginCheckinterceptor).addPathPatterns("/**").excludePathPatterns(arrayList);
    }

    //拓展框架的消息转换器
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>>converters){
        //创建消息转化对象
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter=new MappingJackson2HttpMessageConverter();
        //设置对象转换器，地层加jackson将java对象转化为json
        mappingJackson2HttpMessageConverter.setObjectMapper(new JacksonObjectMapper());
        //将上面的消息转换器对象追加到mvc框架的转换器集合中
        converters.add(0,mappingJackson2HttpMessageConverter);
    }
    //配置文档生成信息
    @Bean
    public Docket createRestApi() {
        // 配置生成的文档信息
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title("瑞吉外卖").version("1.0").description("瑞吉外卖接口文档").build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.itheima.reggie.controller"))
                .paths(PathSelectors.any())
                .build();
    }

}
