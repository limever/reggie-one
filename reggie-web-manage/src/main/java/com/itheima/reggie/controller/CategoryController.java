package com.itheima.reggie.controller;

import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Category;
import com.itheima.reggie.service.CategoryService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(tags = "分类管理")//加载Controller类上,表示对类的说明
@RestController
public class CategoryController {


    @Autowired
    private CategoryService categoryService;
    @ApiOperation("查询所有")
    //查询所有

    @GetMapping("/category/findAll")
    public ResultInfo findall(){
        List<Category>categoryList=categoryService.findAll();
        return ResultInfo.success(categoryList);
    }
    //保存
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "category",value = "保存",required = true)
            }
    )
    @ApiOperation("保存")
    @PostMapping("/category")
    public ResultInfo save(@RequestBody Category category){
        categoryService.save(category);
        return ResultInfo.success(null);

    }
    //更新
    @ApiOperation("更新")
    @PutMapping("/category")
    public ResultInfo updeate(@RequestBody Category category){
        categoryService.update(category);
        return ResultInfo.success(null);

    }
    //删除
    @ApiOperation("删除")
    @DeleteMapping("/category")
    public ResultInfo delete(long id){
        ResultInfo resultInfo=categoryService.delect(id);
        return resultInfo;
    }
    //根据type查询分类信息
    @ApiOperation("根据type查询分类信息")
    @GetMapping("/category/list")
    public ResultInfo findtype(Integer type){
        List<Category>categoryList=categoryService.findtype(type);
        return ResultInfo.success(categoryList);
    }




}