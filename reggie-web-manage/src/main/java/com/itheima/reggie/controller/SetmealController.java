package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Dish;
import com.itheima.reggie.damain.Setmeal;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    //分页查询
    @GetMapping("/setmeal/page")
    public ResultInfo findByPage(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            String name
    ) {
        Page<Setmeal> page = setmealService.findall(pageNum, pageSize, name);
        return ResultInfo.success(page);
    }

    //新增
    @PostMapping("/setmeal")
    public ResultInfo save(@RequestBody Setmeal setmeal) {
        setmealService.save(setmeal);
        return ResultInfo.success(null);
    }

    //删除
    @DeleteMapping("/setmeal")
    public ResultInfo delete(@RequestParam("ids") List<Long> id) {
        setmealService.delete(id);
        return ResultInfo.success(null);
    }

    //根据id插叙套餐
    @GetMapping("/setmeal/{id}")
    public ResultInfo findbyid(@PathVariable("id") Long id) {
        Setmeal setmeal = setmealService.fingbyid(id);
        return ResultInfo.success(setmeal);
    }

    //修改
    @PutMapping("/setmeal")
    public ResultInfo update(@RequestBody Setmeal setmeal) {
        setmealService.update(setmeal);
        return ResultInfo.success(null);
    }

    //起售-停售
    @PostMapping("/setmeal/status/{status}")
    public ResultInfo status(@PathVariable("status")Integer status,
                             @RequestParam("ids") List<Long>id) {

        setmealService.status(status, id);
        return ResultInfo.success(null);
    }
}
