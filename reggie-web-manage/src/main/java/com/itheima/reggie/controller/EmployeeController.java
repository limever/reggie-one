package com.itheima.reggie.controller;

import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Employee;
import com.itheima.reggie.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
/*@RestController 是@controller和@ResponseBody 的结合
@Controller 将当前修饰的类注入SpringBoot IOC容器，使得从该类所在的项目跑起来的过程中，这个类就被实例化。
@ResponseBody 它的作用简短截说就是指该类中所有的API接口返回的数据，甭管你对应的方法返回Map或是其他Object，它会以Json字符串的形式返回给客户端
*/
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    //登录
    @PostMapping("/employee/login")
    public ResultInfo login(HttpSession session, @RequestBody Map<String,String>map){
        //接收参数
        String username=map.get("username");
        String password=map.get("password");
        //调用service登录
        ResultInfo resultInfo=employeeService.login(username,password);
        //将员工信息 保存到session
        if (resultInfo.getCode()==1){
            Employee employee= (Employee) resultInfo.getData();
            session.setAttribute("键值对的键",employee);
        }
        //返回结果
        return resultInfo;
    }
    //列表查询
    @GetMapping("/employee/find")
    public ResultInfo findList(String name) {
        //1. 调用service查询
        List<Employee> employeeList = employeeService.findByName(name);

        //2. 结果返回
        return ResultInfo.success(employeeList);
    }
    //退出
    @PostMapping("/employee/logout")
    public ResultInfo logout(HttpSession session) {
        //1. 注销session
        session.invalidate();

        //2. 返回成功标识
        return ResultInfo.success(null);
    }
    //保存
    @PostMapping("/employee")
    public ResultInfo save(@RequestBody Employee employee){
        employeeService.save(employee);
        return ResultInfo.success(null);
    }
    //查询员工id
    @GetMapping("/employee/{id}")
    public ResultInfo fingbyid(@PathVariable("id") Long id){
        Employee employee=employeeService.fingbyid(id);
        return ResultInfo.success(employee);
    }
    //更新
    @PutMapping("/employee")
    public ResultInfo update(@RequestBody Employee employee){
        employeeService.update(employee);
        return ResultInfo.success(null);
    }


}
