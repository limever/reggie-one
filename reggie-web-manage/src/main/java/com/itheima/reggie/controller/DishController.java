package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Dish;
import com.itheima.reggie.service.DishControllerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(tags = "菜品信息")
@RestController
public class DishController {
    @Autowired
    private DishControllerService dishControllerService;
    @Autowired
    private RedisTemplate redisTemplate;

    //分页查询
    //@Cacheable 一般标注在查询方法上，表示在方法执行前，先查看缓存中是否有数据，
    //         如果有直接返回，
    //         若没有，调用方法查询并将方法返回值放到缓存中
    @ApiOperation("分页查询")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "page",value = "页码",required = true),
                    @ApiImplicitParam(name = "pagesize",value = "页数",required = false),
                    @ApiImplicitParam(name = "name",value = "菜品名称",required = false)
            }

    )
    @Cacheable(value = "id",key ="'all'" )
    @GetMapping("/dish/page")
    public ResultInfo  findBypage(@RequestParam(value = "page",defaultValue = "1") Integer pageNum,
                                  @RequestParam(defaultValue = "10") Integer pageSize,String name){
        Page page=dishControllerService.findByPage(pageNum,pageSize,name);
        return ResultInfo.success(page);

    }

    //根据id查菜品信息
    @Cacheable(value = "id",key = "#id")
    @GetMapping("/dish/{id}")
    public ResultInfo updateByid(@PathVariable("id")Long id){
        Dish dish=dishControllerService.updateByid(id);
        return ResultInfo.success(dish);
    }



    //以下优化时需要清除redis缓存




    //新增
    //@CachePut用于将方法返回值，放入缓存，主要有下面两个属性：
    //  value: 缓存的名称，每个缓存名称下面可以有很多key
    //  key: 缓存的key，支持Spring的表达式语言SPEL语法
    //缓存的键:   value::key    key的写法如下：
    // #参数名：获取方法参数的变量
    // #result：获取方法返回值中的变量
    @CachePut(value = "dish",key = "#result.id")
    @PostMapping("/dish")
    public ResultInfo save(@RequestBody Dish dish){

//      清理掉redis的缓存数据
//        redisTemplate.delete(redisTemplate.keys("diash_*"));
        dishControllerService.save(dish);
        return ResultInfo.success(null);
    }


    //修改，将内容先删后加
    @PutMapping("/dish")
    public ResultInfo update(@RequestBody Dish dish){
        dishControllerService.update(dish);
        return ResultInfo.success(dish);
    }
//删除-批量
    @DeleteMapping("/dish")
    public  ResultInfo delate(@RequestParam("ids")List<Long> id){
        dishControllerService.delete(id);
        return ResultInfo.success(null);
    }
    //停售
    @PostMapping("//dish/status/{status}")
    public ResultInfo status(@PathVariable("status")Integer status,
                             @RequestParam("ids") List<Long>id){

       dishControllerService.status(status,id);
        return ResultInfo.success(null);
    }


    //新增
    @GetMapping("/dish/list")
    public ResultInfo findlist(Long categoryId,String name){
        List<Dish>dishList=dishControllerService.findlist(categoryId,name);
        return ResultInfo.success(dishList);
    }

}
