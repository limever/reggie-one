package com.itheima.reggie;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
//指定持久层接口所在包
@MapperScan("com.itheima.reggie.mapper")
//开启声明事务
@EnableTransactionManagement
//开启自动配置
@SpringBootApplication
//等于private  final Logger logger = LoggerFactory.getLogger(XXX.class);
@Slf4j
@EnableCaching//开启基于注解的缓存
public class WebManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebManageApplication.class,args);
        //测试启动成功
//        log.info("启动成功！");
        System.out.println("启动成功！");

    }
}
