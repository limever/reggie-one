package com.itheima.reggie.test;

import com.itheima.reggie.common.OssTemplate;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
@Slf4j
public class osstest {

    @Autowired
    private OssTemplate ossTemplate;
    @Test
    public void test() throws FileNotFoundException {
        String filepath=ossTemplate.upload("libai.jpg",new FileInputStream("G:\\李白.jpg"));
        log.info("上传到："+filepath);
    }

}
