package com.itheima.reggie.service;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Category;

import java.util.List;

public interface CategoryService {

    //查询所有
    List<Category> findAll();


    void save(Category category);
//更新
    void update(Category category);
//删除分类
    ResultInfo delect(long id);
//根据type分类查询
    List<Category> findtype(Integer type);
}
