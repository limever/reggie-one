package com.itheima.reggie.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Employee;
import com.itheima.reggie.mapper.EmployeeMapper;
import com.itheima.reggie.service.EmployeeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EmployeeServiceImpl  implements EmployeeService  {
@Autowired
private EmployeeMapper employeeMapper;
    @Override
    public ResultInfo login(String username, String password) {
        Employee employee=employeeMapper.findbyusername(username);
        String passwordwithMD5= SecureUtil.md5(password);
       //根据username查询，查不到返回用户不存在
        if (employee==null){
            return ResultInfo.error("用户名不存在");
        }else if (!StringUtils.equals(passwordwithMD5,employee.getPassword())){
            return ResultInfo.error("密码错误");
        }else if (employee.getStatus().equals(Employee.STATUS_DISABLE)){
            return ResultInfo.error("当前用户被禁用");
        }else {
            return ResultInfo.success(employee);
        }
    }
//按名字查询
    @Override
    public List<Employee> findByName(String name) {

        return employeeMapper.findbyname(name);
    }
//保存
    @Override
    public void save(Employee employee) {
        //接受参数，加雪花算法生成唯一id
        employee.setId(IdUtil.getSnowflake(1,1).nextId());
        //设置默认密码,加密
        employee.setPassword(SecureUtil.md5("123456"));
        //默认status激活
        employee.setStatus(Employee.STATUS_ENABLE);
        //时间设置
//        employee.setCreateTime(new Date());
//        employee.setUpdateTime(new Date());
        //操作人设置
//        employee.setCreateUser(1L);
//        employee.setUpdateUser(1L);

        //调用mapper
        employeeMapper.save(employee);
    }

    @Override
    public Employee fingbyid(Long id) {
        return employeeMapper.fingbyid(id);
    }

    @Override
    public void update(Employee employee) {
        //重新赋值参数
//        employee.setUpdateTime(new Date());
//        employee.setUpdateUser(1L);
        //执行修改
        employeeMapper.update(employee);
    }


}
