package com.itheima.reggie.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.damain.Dish;
import com.itheima.reggie.damain.DishFlavor;
import com.itheima.reggie.mapper.CategoryMapper;
import com.itheima.reggie.mapper.DishFlavorMapper;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.service.DishControllerService;
import com.itheima.reggie.damain.Category;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.security.krb5.internal.rcache.DflCache;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static javafx.scene.input.KeyCode.*;

@Service
@Transactional
public class DishControllerServiceImpl implements DishControllerService {
    private final DishFlavorMapper dishFlavorMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private DishMapper dishMapper;

    public DishControllerServiceImpl(DishFlavorMapper dishFlavorMapper) {
        this.dishFlavorMapper = dishFlavorMapper;
    }

    @Override
    public Page findByPage(Integer pageNum, Integer pageSize, String name) {
        //查询菜品信息
        //设置分页条件
        Page<Dish>page=new Page<>(pageNum,pageSize);
        //设置业务条件
        LambdaQueryWrapper<Dish>lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(StringUtils.isNoneEmpty(name),Dish::getName,name);
        //执行查询
        page=dishMapper.selectPage(page,lambdaQueryWrapper);
        //遍历分页列表，得到每一个菜品
        List<Dish>dishList=page.getRecords();
        if (CollectionUtil.isNotEmpty(dishList)){
            for (Dish dish:dishList){
                //在根据菜品categoryid查询他的分类名称
             Category category=categoryMapper.selectById(dish.getCategoryId());
             dish.setCategoryName(category.getName());

            }
        }
        System.out.println(page);
        return page;
    }
//保存
    @Override
    public void save(Dish dish) {
        //保存菜品基本信息
        dishMapper.insert(dish);
                //获取口味列表，遍历,if for
        if (CollectionUtil.isNotEmpty(dish.getFlavors())){
            for (DishFlavor dishFlavor:dish.getFlavors()
                 ) {
                //设置菜品id
                dishFlavor.setDishId(dish.getId());
                //保存口味
                dishFlavorMapper.insert(dishFlavor);
            }
        }
    }

    @Override
    public Dish updateByid(Long id) {
        //根据id从菜品表中进行主键查询，获取详细信息
        Dish dish=dishMapper.selectById(id);
        //根据菜品category查询分类名称
        Category category=categoryMapper.selectById(dish.getCategoryId());
        dish.setCategoryName(category.getName());
        //根据菜品的id查询口味
        LambdaQueryWrapper<DishFlavor>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(DishFlavor::getDishId,dish.getName());
        List<DishFlavor>dishFlavorList=dishFlavorMapper.selectList(wrapper);
        dish.setFlavors(dishFlavorList);
//        返回
        return dish;

    }
//修改
    @Override
    public void update(Dish dish) {
        //根据菜品id修改菜品表信息
        dishMapper.updateById(dish);
        //根据菜品id删除口味的信息
        LambdaQueryWrapper<DishFlavor> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(DishFlavor::getDishId,dish.getId());
        dishFlavorMapper.delete(wrapper);
        //遍历前端传入的口味列表，重新保存
        if (CollectionUtil.isNotEmpty(dish.getFlavors())){
            for (DishFlavor flavor:dish.getFlavors()){
                flavor.setDishId(dish.getId());
                dishFlavorMapper.insert(flavor);
            }
        }

    }
//批量删除
    @Override
    public void delete(List<Long> id) {
        //如果其中一个是在售，则全部事务回滚

        LambdaQueryWrapper<Dish>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Dish::getStatus,1);
        wrapper.in(Dish::getId,id);
        Integer count=dishMapper.selectCount(wrapper);
        if (count>0){
            throw new CustomException("当前菜品部分在售，无法删除");
        }
        if (CollectionUtil.isNotEmpty(id)){
            for (Long i:id) {
                LambdaQueryWrapper<DishFlavor>wrapper1=new LambdaQueryWrapper<>();
                wrapper1.eq(DishFlavor::getDishId,i);
                dishFlavorMapper.delete(wrapper1);

                dishMapper.deleteById(i);
            }
        }

    }

//    @Override
//    public void status(long id) {
//        //查id
//        dishMapper.selectById(id);
//        LambdaQueryWrapper<Dish>wrapper=new LambdaQueryWrapper<>();
//        wrapper.eq(Dish::getStatus,1);
////        dishFlavorMapper.update();
//
//
//    }



    @Override
    public List<Dish> findlist(Long categoryId,String name) {
        //创建疯转类
        LambdaQueryWrapper<Dish>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Dish::getStatus,1)
                .eq(categoryId!=null,Dish::getCategoryId,categoryId)
                .like(StringUtils.isNotEmpty(name),Dish::getName,name);

        //执行查询
        List<Dish>dishList=dishMapper.selectList(wrapper);
        //查询菜品信息 查询每个菜品的口味信息
        if (CollectionUtil.isNotEmpty(dishList)) {
            for (Dish dish:dishList
                 ) {
                LambdaQueryWrapper<DishFlavor>wrapper1=new LambdaQueryWrapper<>();
                wrapper1.eq(DishFlavor::getDishId,dish.getId());
                List<DishFlavor>dishFlavorList=dishFlavorMapper.selectList(wrapper1);
                dish.setFlavors(dishFlavorList);
            }
        }
        return dishMapper.selectList(wrapper);
    }
//停售-起售
    @Override
    public void status(Integer status, List<Long> id) {
        LambdaQueryWrapper<Dish>wrapper=new LambdaQueryWrapper<>();
        wrapper.in(Dish::getId,id);
        //设置set部分
        Dish dish=new Dish();
        dish.setStatus(status);
        dishMapper.update(dish,wrapper);
    }

}
