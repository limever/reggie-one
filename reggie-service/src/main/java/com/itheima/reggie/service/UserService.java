package com.itheima.reggie.service;

import com.itheima.reggie.common.ResultInfo;

public interface UserService {
    //登录
    ResultInfo login(String phone, String code);
//发送短信
    void sendSms(String phone);
//退出
    void remove(String token);
}
