package com.itheima.reggie.service;

import com.itheima.reggie.damain.Cart;

import java.util.List;

public interface CartService {
    //添加购物车
    Cart add(Cart cart);
    //查询指定用户的购物车列表
    List<Cart> findList();

    //清空购物车
    void clean();
//减少购物车内菜品
    Cart sub(Cart cart);
}
