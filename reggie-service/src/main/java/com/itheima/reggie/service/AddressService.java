package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.damain.Address;

import java.util.List;

public interface AddressService {

    Page<Address> findone(Integer pageNum, Integer pageSize);

    //增加地址
    void save(Address address);
//查询当前id所有地址
    List<Address> select();

//设置默认地址
    void defaut(Long id);
//设置主键查询
    Address findbyid(Long id);
//更新
    void update(Address address);
//删除
    void delete(List<Long> id);
//默认地址查询
    Address getdefaut();

}
