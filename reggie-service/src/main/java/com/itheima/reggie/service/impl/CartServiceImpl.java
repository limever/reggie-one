package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.reggie.common.UserHolder;
import com.itheima.reggie.damain.Cart;
import com.itheima.reggie.mapper.CartMapper;
import com.itheima.reggie.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CartServiceImpl implements CartService {
    @Autowired
    private CartMapper cartMapper;
    //添加购物车
    @Override
    public Cart add(Cart cart) {
       //先判断当前添加的菜品和套餐是否在遍欧洲高存在
        LambdaQueryWrapper<Cart>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Cart::getUserId, UserHolder.get().getId())
                .eq(cart.getDishId()!=null,Cart::getDishId,cart.getDishId())
                .eq(cart.getSetmealId()!=null,Cart::getSetmealId,cart.getSetmealId());
        Cart cartfordb=cartMapper.selectOne(wrapper);
        if (cartfordb!=null){
            //如果存在，则number+1
            cartfordb.setNumber(cartfordb.getNumber()+1);
            cartMapper.updateById(cartfordb);
            return cartfordb;
        }else {
            //当前添加的不存在，则新建一个cart，保存到库中
            cart.setUserId(UserHolder.get().getId());
            cart.setNumber(1);
            cart.setCreateTime(new Date());
            cartMapper.insert(cart);
            return cart;

        }
    }

    @Override
    public List<Cart> findList() {
        //select * from shopping_cart where user_id = #{登录用户id}
        LambdaQueryWrapper<Cart> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Cart::getUserId,UserHolder.get().getId());

        return cartMapper.selectList(wrapper);
    }
//清空购物车
    @Override
    public void clean() {
        //delete from shopping_cart where user_id =  = #{登录用户id}
        LambdaQueryWrapper<Cart> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Cart::getUserId,UserHolder.get().getId());
        cartMapper.delete(wrapper);
    }

    @Override
    public Cart sub(Cart cart) {
        //用id确定需要更新的购物车
        Long dishid=cart.getDishId();
        Long setmealid=cart.getSetmealId();

        LambdaQueryWrapper<Cart>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(dishid!=null,Cart::getDishId,dishid)
                .eq(setmealid!=null,Cart::getSetmealId,setmealid)
                .eq(Cart::getUserId,UserHolder.get().getId());
        Cart cart1=cartMapper.selectOne(wrapper);
        //先判断购物车内菜品是否存在，有则减1,为0 删除菜品
        if (cart1!=null){
            cart1.setNumber(cart1.getNumber()-1);
            if (cart1.getNumber()<=0){
                cartMapper.deleteById(cart1.getId());
            }else {
                cartMapper.updateById(cart1);
            }

        }
        return cart1;

//        wrapper.eq(Cart::getDishId,cart.getDishId())
    }
    /*//先判断当前添加的菜品和套餐是否在遍欧洲高存在
        LambdaQueryWrapper<Cart>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Cart::getUserId, UserHolder.get().getId())
                .eq(cart.getDishId()!=null,Cart::getDishId,cart.getDishId())
                .eq(cart.getSetmealId()!=null,Cart::getSetmealId,cart.getSetmealId());
        Cart cartfordb=cartMapper.selectOne(wrapper);
        if (cartfordb!=null){
            //如果存在，则number+1
            cartfordb.setNumber(cartfordb.getNumber()+1);
            cartMapper.updateById(cartfordb);
            return cartfordb;
        }else {
            //当前添加的不存在，则新建一个cart，保存到库中
            cart.setUserId(UserHolder.get().getId());
            cart.setNumber(1);
            cart.setCreateTime(new Date());
            cartMapper.insert(cart);
            return cart;

        }*/

}
