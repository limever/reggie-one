package com.itheima.reggie.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.JwtUtil;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.common.SmsTemplate;
import com.itheima.reggie.damain.User;
import com.itheima.reggie.mapper.CartMapper;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.UserService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SmsTemplate smsTemplate;
    @Autowired
    private CartMapper cartMapper;

    @Override
    public ResultInfo login(String phone, String code) {
        //对比验证码，如果错误返回提示
        String codeforredis = (String) redisTemplate.opsForValue().get("code-" + phone);
        if (!StringUtils.equals(code, codeforredis)) {
            throw new CustomException("验证码错误");
        }
        //根据手机号查询用户是否存在，
        //查到后，判断status是否为1，否则提示已禁用
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.isNotEmpty(phone), User::getPhone, phone);
        //将wrapper放入user对象
        User user = userMapper.selectOne(wrapper);
        //查询是否存在
        if (user != null) {
            if (user.getStatus() != 1) {
                throw new CustomException("该用户已被禁用");
            }
        } else {
            //不存在则添加
            //没有对象，new一个
            user = new User();
            //将xxoo保存到对象中
            user.setPhone(phone);
            user.setStatus(1);
            // 传入phone（set方法获取，将其insert到usermapper中
            userMapper.insert(user);
        }
        //生成token
        Map map=new HashMap();
        map.put("id",user.getId());
        String token= JwtUtil.createToken(map);
        //将token和信息保存到redis,设置时间1天
        redisTemplate.opsForValue().set("token-"+token,user,1,TimeUnit.DAYS);
        return ResultInfo.success(token);
    }

    //发送短信
    @Override
    public void sendSms(String phone) {

        //生成六位验证码
        //todo 测试模式
        String code="123456";
        System.out.println("验证码为:"+code);
//        String code= RandomUtil.randomNumbers(6);
        //调用redis保存一下,保存五分钟
        redisTemplate.opsForValue().set("code-"+phone,code,5, TimeUnit.MINUTES);
        //调用阿里云发送
//        smsTemplate.sendSms(phone,code);
    }

    @Override
    public void remove(String token) {
        redisTemplate.delete("token-"+token);
    }
}
