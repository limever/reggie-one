package com.itheima.reggie.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.damain.Category;
import com.itheima.reggie.damain.Dish;
import com.itheima.reggie.damain.Setmeal;
import com.itheima.reggie.damain.SetmealDish;
import com.itheima.reggie.mapper.CategoryMapper;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.mapper.SetmealDishMapper;
import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.SetmealService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

//套餐
@Service
@Transactional
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishMapper dishMapper;


    @Override
    public Page<Setmeal> findall(Integer pageNum, Integer pageSize, String name) {
        //分页查询
        Page<Setmeal> page = new Page<>(pageNum, pageSize);
        //业务查询
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(name), Setmeal::getName, name);
        //查询执行
        page = setmealMapper.selectPage(page, wrapper);
        //变量列表，获取每个套餐
//先判断不为空
        if (CollectionUtil.isNotEmpty(page.getRecords())) {
            //加循环，得到套餐信息
            for (Setmeal setmeal : page.getRecords()) {
                Category category = categoryMapper.selectById(setmeal.getCategoryId());
                setmeal.setCategoryName(category.getName());

            }
        }
        return page;

        //根据套餐category-id获取表中内容
    }

    //保存
    @Override
    public void save(Setmeal setmeal) {
//保存基本信息到setmeal表
        setmealMapper.insert(setmeal);
        //获取菜品列表
        List<SetmealDish> setmealDishes = setmeal.getSetmealDishes();
        //判断不为空，
        if (CollectionUtil.isNotEmpty(setmealDishes)) {
            int i = 0;
            //输出草滩信息
            for (SetmealDish setmealDish : setmealDishes
            ) {
                setmealDish.setSetmealId(setmeal.getId());
                setmealDish.setSort(i++);
                setmealDishMapper.insert(setmealDish);
            }
        }
    }

    @Override
    public void delete(List<Long> id) {
        //判断status是否为0
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Setmeal::getStatus, 1)
                .in(Setmeal::getId, id);
        Integer num = setmealMapper.selectCount(wrapper);
        if (num > 0) {
            throw new CustomException("有套餐正处于售卖中，无法删除");
        } else {
            //删除setmeal-dish中的setmeal-id在id中的（条件删除
            //构建删除条件
            LambdaQueryWrapper<SetmealDish> wrapper1 = new LambdaQueryWrapper<>();
            wrapper1.in(SetmealDish::getSetmealId, id);
            setmealDishMapper.delete(wrapper1);
            //再删除id在id中的内容
            setmealMapper.deleteBatchIds(id);
        }
    }

    @Override
    public Setmeal fingbyid(Long id) {
        //查询id信息-套餐表
        Setmeal setmeal = setmealMapper.selectById(id);
        //根据套餐的category-id到category表中找分类名
        Category category = categoryMapper.selectById(setmeal.getCategoryId());
        setmeal.setCategoryName(category.getName());
        //根据套餐id到setmeal中查询套餐信息
        LambdaQueryWrapper<SetmealDish> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SetmealDish::getSetmealId, setmeal.getId());
        List<SetmealDish> setmealDishes = setmealDishMapper.selectList(wrapper);
        setmeal.setSetmealDishes(setmealDishes);
        return setmeal;
    }

    @Override
    public void update(Setmeal setmeal) {
        //根据套餐ID修改基本信息到setmeal表
        setmealMapper.updateById(setmeal);
        //根据setmeal-id删除setmeal-dish中的型关信息（条件删除
        LambdaQueryWrapper<SetmealDish> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SetmealDish::getSetmealId, setmeal.getId());
        setmealDishMapper.delete(wrapper);

        //重新天健
        List<SetmealDish> setmealDishes = setmeal.getSetmealDishes();
        //啊判断非空
        if (CollectionUtil.isNotEmpty(setmealDishes)) {
            int i = 0;
            //循环
            for (SetmealDish setmealDish : setmealDishes) {
                setmealDish.setSetmealId(setmeal.getId());//设置套餐id
                setmealDish.setSort(i++);//份数加1
                setmealDishMapper.insert(setmealDish);

            }
        }
    }

    @Override
    public void status(Integer status, List<Long> id) {
        LambdaQueryWrapper<Setmeal>wrapper=new LambdaQueryWrapper<>();
        wrapper.in(Setmeal::getId,id);

        Setmeal setmeal=new Setmeal();
        setmeal.setStatus(status);
        setmealMapper.update(setmeal,wrapper);

    }
//查询指定套餐下的菜品列表
    @Override
    public List<Dish> findDishList(Long id) {
        //先套餐id查询出当前套餐下的菜品集合
        LambdaQueryWrapper<SetmealDish>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(SetmealDish::getSetmealId,id);
        List<SetmealDish>setmealDishes=setmealDishMapper.selectList(wrapper);
        //遍历集合
        List<Dish>dishList=new ArrayList<Dish>();
        if (CollectionUtil.isNotEmpty(setmealDishes)) {
            for (SetmealDish setmealDish : setmealDishes) {
              Dish dish=dishMapper.selectById(setmealDish.getDishId());
              dish.setCopies(setmealDish.getCopies());
              dishList.add(dish);
            }
        }

        return dishList;
    }

    @Override
    public List<Setmeal> findList(Long categoryId, Integer status) {
        LambdaQueryWrapper<Setmeal>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Setmeal::getCategoryId,categoryId)
                .eq(Setmeal::getStatus,status);
        return setmealMapper.selectList(wrapper);
    }


}
