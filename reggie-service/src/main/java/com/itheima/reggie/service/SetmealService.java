package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.damain.Dish;
import com.itheima.reggie.damain.Setmeal;

import java.util.List;

//套餐
public interface SetmealService {
    //查询
    Page<Setmeal> findall(Integer pageNum, Integer pageSize, String name);

//保存
    void save(Setmeal setmeal);
//删除
    void delete(List<Long> id);
//根据id回显信息
    Setmeal fingbyid(Long id);
//修改
    void update(Setmeal setmeal);
//起售-停售
    void status(Integer status, List<Long> id);
//查询指定套餐下的菜品列表
    List<Dish> findDishList(Long id);
//查询指定分类下的套餐
    List<Setmeal> findList(Long categoryId, Integer status);
}
