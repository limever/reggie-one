package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.damain.Dish;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DishControllerService {
    //根据name分页查询
    Page findByPage(Integer pageNum, Integer pageSize, String name);
//保存
    void save(Dish dish);
//根据id更新
    Dish updateByid(Long id);
    //修改
    void update(Dish dish);
//批量删除
    void delete(List<Long> id);


//查询
    List<Dish> findlist(Long categoryId, String name);
    //status修改
    void status(Integer status, List<Long> id);
}
