package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.damain.Order;

import java.util.Date;

public interface OrderService {




    //提交订单
    void save(Order order);
//分页查询
    Page<Order> findpage(Integer pageNum, Integer pageSize);
//分页条件查询
    Page<Order> findbypage(Integer pageNum, Integer pageSize, String number, Date beginTime, Date endTime);
}
