package com.itheima.reggie.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Employee;

import java.util.List;


public interface EmployeeService  {
//登录
    ResultInfo login(String username, String password);

    List<Employee> findByName(String name);

    //保存
    void save(Employee employee);
//根据id查询
    Employee fingbyid(Long id);
//更新员工信息
    void update(Employee employee);
}
