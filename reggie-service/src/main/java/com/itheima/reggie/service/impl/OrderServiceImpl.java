package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.apache.commons.lang3.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.UserHolder;
import com.itheima.reggie.damain.Address;
import com.itheima.reggie.damain.Cart;
import com.itheima.reggie.damain.Order;
import com.itheima.reggie.damain.OrderDetail;
import com.itheima.reggie.mapper.OrderDetailMapper;
import com.itheima.reggie.mapper.OrderMapper;
import com.itheima.reggie.service.AddressService;
import com.itheima.reggie.service.CartService;
import com.itheima.reggie.service.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private AddressService addressService;
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private CartService cartService;

    //提交订单
    @Override
    public void save(Order order) {
      //1.准备数据
//        根据地址信息获取用户的购物车信息
//        用list数组查询当前用户的购物车信息
//        2.手动生成订单-订单唯一id，雪花-idworker
//        生成订单详情，利用copy cart表中的菜品数据
//        之后设置单独的id和orderID id用雪花算法，orderID自动生成
//
//        在copy循环之前设置sum用于金额的计算，使用DigDecimal，精度计算
//        在循环中用sum循环计算金额，cart中获取单价amount，乘multiply cart中的数量number
//        最后将订单保存到mapper中
//        3，订单生成，用order获取表中的属性

//        清空购物车
        BigDecimal sum=new BigDecimal(0);

        Address address=  addressService.findbyid(order.getAddressId());
//
        List<Cart>cartList=cartService.findList();
         long orderid= IdWorker.getId();
        for (Cart cart:cartList
             ) {
            OrderDetail orderDetail=new OrderDetail();
            BeanUtils.copyProperties(cart,orderDetail);

            orderDetail.setId(IdWorker.getId());
            orderDetail.setOrderId(orderid);
            sum=sum.add(cart.getAmount().multiply(new BigDecimal(cart.getNumber())));
            orderDetailMapper.insert(orderDetail);

        }
        //生成订单
        order.setId(orderid);
        order.setNumber(orderid+"");
        order.setStatus(1);//待付款
        order.setUserId(UserHolder.get().getId());
        order.setOrderTime(new Date());
        order.setCheckoutTime(new Date());
        order.setAmount(sum);
        order.setUserName(UserHolder.get().getName());
        order.setPhone(address.getPhone());
        order.setAddress(address.getDetail());
        order.setConsignee(address.getConsignee());
        orderMapper.insert(order);
        //清理购物车
        cartService.clean();

    }
//分页
    @Override
    public Page<Order> findpage(Integer pageNum, Integer pageSize) {

        //设置分页条件

        Page<Order>page=new Page<>(pageNum,pageSize);

        //设置业务条件
        LambdaQueryWrapper<Order>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Order::getUserId,UserHolder.get().getId())
                .orderByDesc(Order::getOrderTime);

        //执行查询
        page=orderMapper.selectPage(page,wrapper);

        //获取每个订单
        if (CollectionUtils.isNotEmpty(page.getRecords())){
            for (Order order:page.getRecords()
                 ) {
                LambdaQueryWrapper<OrderDetail>wrapper1=new LambdaQueryWrapper<>();
                wrapper1.eq(OrderDetail::getOrderId,order.getId());
                List<OrderDetail>orderDetails=orderDetailMapper.selectList(wrapper1);
                order.setOrderDetails(orderDetails);
            }
        }
        return page;
    }
//分页条件查询
    @Override
    public Page<Order> findbypage(Integer pageNum, Integer pageSize, String number, Date beginTime, Date endTime) {
        //设置分页条件查询
        Page<Order>page=new Page<>(pageNum,pageSize);
        //设置业务条件查询
        LambdaQueryWrapper<Order>wrapper=new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(number),Order::getNumber,number)
                .between(beginTime!=null,Order::getOrderTime,beginTime,endTime);

        return orderMapper.selectPage(page,wrapper);

    }
}
