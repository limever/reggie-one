package com.itheima.reggie.handler;

import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.ResultInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.DuplicateFormatFlagsException;

//全局异常类
@ControllerAdvice
@ResponseBody
//异常处理类，处理三类异常，1，用户可识别异常
//2，自定义异常
//3，非预期异常
public class GlobalExceptionHandler {
//处理唯一值重复的异常
    @ExceptionHandler(DuplicateFormatFlagsException.class)
    public ResultInfo chongfuyichang(DuplicateFormatFlagsException e){
        //打印日志
        e.printStackTrace();
        if (e.getMessage().contains("idx_category_name")){
            return ResultInfo.error("分类名称重复");
        }
        //给前端一个提示
        return ResultInfo.error("填写的值重复");
    }
    //处理自定义异常
    @ExceptionHandler(CustomException.class)
    public ResultInfo zidingyiyichcang(CustomException e){
        //打印日志
        e.printStackTrace();
       //给前台一个提示
        return  ResultInfo.error(e.getMessage());
    }
    @ExceptionHandler(Exception.class)
    public ResultInfo feiyuqiyichang(Exception e){
        e.printStackTrace();
        //返回提示到前端
        return ResultInfo.error("服务器坏了，有人删库跑路");
    }

    }