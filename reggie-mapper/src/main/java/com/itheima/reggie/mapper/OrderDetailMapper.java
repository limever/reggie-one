package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.damain.OrderDetail;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}
