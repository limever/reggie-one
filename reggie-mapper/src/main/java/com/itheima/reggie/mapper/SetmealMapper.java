package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.damain.Setmeal;
import org.springframework.stereotype.Repository;

@Repository
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
