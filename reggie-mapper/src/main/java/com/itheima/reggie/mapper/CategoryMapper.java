package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryMapper extends BaseMapper<Category> {
    List<Category> findall();

    void save(Category category);

    void update(Category category);
//删除
    void delete(long id);
//删除菜品时查询菜品数量
    Integer countdish(long id);
//删除套餐时查询内部套餐数量
    Integer countmeal(long id);
}