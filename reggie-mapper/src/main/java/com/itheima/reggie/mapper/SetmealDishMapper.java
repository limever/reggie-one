package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.damain.SetmealDish;
import org.springframework.stereotype.Repository;

@Repository
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
