package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.damain.Employee;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeMapper extends BaseMapper<Employee> {

    Employee findbyusername(String username);

    List<Employee> findbyname(String name);
//保存
    void save(Employee employee);
//调用id查询
    Employee fingbyid(Long id);

    void update(Employee employee);
}
