package com.itheima.reggie.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.itheima.reggie.common.EmployeeHolder;
import com.itheima.reggie.common.UserHolder;
import com.itheima.reggie.damain.Employee;
import com.itheima.reggie.damain.User;
import lombok.extern.slf4j.Slf4j;

import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    //插入操作，自动填充
    @Override
    public void insertFill(MetaObject metaObject) {
        metaObject.setValue("createTime",new Date());
        metaObject.setValue("updateTime",new Date());
        Employee employee= EmployeeHolder.getThreadLocal();
        if (employee!=null){
            metaObject.setValue("createUser",employee.getId());
            metaObject.setValue("updateUser",employee.getId());
        }
        User user = UserHolder.get();
        if (user != null) {
            metaObject.setValue("createUser", user.getId());
            metaObject.setValue("updateUser", user.getId());
        }

//        metaObject.setValue("createUser",1L);
//        metaObject.setValue("updateUser",1L);
    }
//更新操作
    @Override
    public void updateFill(MetaObject metaObject) {
        metaObject.setValue("updateTime",new Date());
        Employee employee= EmployeeHolder.getThreadLocal();
        if (employee!=null){
//            metaObject.setValue("createUser",employee.getId());
            metaObject.setValue("updateUser",employee.getId());
        }
//        metaObject.setValue("updateUser",1L);
        User user = UserHolder.get();
        if (user != null) {
            metaObject.setValue("updateUser", user.getId());
        }

    }
}
