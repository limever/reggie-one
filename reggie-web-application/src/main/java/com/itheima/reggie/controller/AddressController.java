package com.itheima.reggie.controller;

import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Address;
import com.itheima.reggie.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {
    @Autowired
    private AddressService addressService;
//地址查询,查询当前id所有
    @GetMapping("/address/list")
    public ResultInfo addressfindbyid(){
       List<Address>addressList=addressService.select();
        return ResultInfo.success(addressList);

    }
//新增地址
    @PostMapping("/address")
    public ResultInfo addressadd(@RequestBody Address address){
        addressService.save(address);
        return ResultInfo.success(null);
    }
    //设置默认地址
    @PutMapping("/address/default")
    public ResultInfo defaut(@RequestBody Address address){
        addressService.defaut(address.getId());
        return ResultInfo.success(null);
    }
    //主键id查询
    @GetMapping("/address/{id}")
    public ResultInfo findbyid(@PathVariable("id")Long id){
        return ResultInfo.success(addressService.findbyid(id));
    }
//更新
    @PutMapping("/address")
    public ResultInfo update(@RequestBody Address address){
        addressService.update(address);
        return ResultInfo.success(null);
    }
    //删除
    @DeleteMapping("/address")
    public ResultInfo delete(@RequestParam("ids") List<Long>id){
        addressService.delete(id);
        return ResultInfo.success(null);
    }
    //下单时查询默认地址
    @GetMapping("/address/default")
    public ResultInfo selectbyid(){

        if (addressService.getdefaut()!=null){
            return ResultInfo.success(addressService.getdefaut());
        }else {
            return ResultInfo.error("用户未设置默认地址");
        }
    }
}
