package com.itheima.reggie.controller;

import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class UserController {
    @Autowired
    private UserService userService;
    //验证码发送
    @PostMapping("/user/sendMsg")
    public ResultInfo sendSms(@RequestBody Map<String, String> map) {
        //1. 接收参数
        String phone = map.get("phone");

        //2. 调用service
        userService.sendSms(phone);

        return ResultInfo.success("发送短信成功");
    }
    //登录
    @PostMapping("/user/login")
    public ResultInfo login(@RequestBody Map<String ,String>map){
        //获取参数
        String phone=map.get("phone");
        String code=map.get("code");
        //地so用service进行登录
        ResultInfo resultInfo=userService.login(phone,code);
        return resultInfo;
    }
    //退出
    @PostMapping("/user/logout")
    public ResultInfo logout(@RequestHeader("Authorization") String token){
        //获取token
        token=token.replace("Bearer", "").trim();
        //调用service清除redis中当前token信息
        userService.remove(token);
        //返回结果
        return ResultInfo.success(null);

    }
}
