package com.itheima.reggie.controller;


import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Dish;
import com.itheima.reggie.damain.Setmeal;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SetmealController {
    @Autowired
    private SetmealService setmealService;
    //指定套餐下的分类列表
    @GetMapping("/setmeal/list")
    public ResultInfo findList(Long categoryId, Integer status){
        List<Setmeal>setmealList=setmealService.findList(categoryId,status);
        return ResultInfo.success(setmealList);
    }

    //套餐下的菜品列表
    @GetMapping("/setmeal/list/{id}")
    public ResultInfo setmeal(@PathVariable("id") Long id){
        List<Dish>dishList=setmealService.findDishList(id);
        return ResultInfo.success(dishList);
    }
}
