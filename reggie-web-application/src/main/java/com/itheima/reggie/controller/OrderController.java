package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.ResultInfo;
import com.itheima.reggie.damain.Order;
import com.itheima.reggie.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {
@Autowired
    private OrderService orderService;

    //分页查询所有地址
    @GetMapping("/order/userPage")
    public ResultInfo findall(
            @RequestParam(value = "page",defaultValue ="1" ) Integer pageNum,
            @RequestParam(defaultValue = "5")Integer pageSize
    ){
        Page<Order> page = orderService.findpage(pageNum, pageSize);
        return ResultInfo.success(page);
    }


    //提交订单
    @PostMapping("/order/submit")
    public ResultInfo sub(@RequestBody Order order){
        orderService.save(order);
        return ResultInfo.success(null);
    }

}
