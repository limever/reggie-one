package com.itheima.reggie.common;

import com.itheima.reggie.damain.Employee;

public class EmployeeHolder {
    //维护ThreadLocal对象
    private static ThreadLocal<Employee>threadLocal=new ThreadLocal<Employee>();
    //放入User
    public static void setThreadLocal(Employee employee){
        threadLocal.set(employee);
    }
    //获取User
    public static Employee getThreadLocal(){
        return threadLocal.get();
    }
    //移除User
    public static void remove(){
        threadLocal.remove();
    }

}
