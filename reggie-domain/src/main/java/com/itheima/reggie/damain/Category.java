package com.itheima.reggie.damain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

//分类
@ApiModel
@Data
public class Category implements Serializable {

    public static final Integer TYPE_DISH = 1;
    public static final Integer TYPE_SETMEAL = 2;
    @ApiModelProperty("主键")
    private Long id;//主键

    @ApiModelProperty("类型 1 菜品分类 2 套餐分类")
    private Integer type;//类型 1 菜品分类 2 套餐分类

    @ApiModelProperty("分类名称")
    private String name;//分类名称

    @ApiModelProperty("顺序")
    private Integer sort; //顺序

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;//更新时间

    @ApiModelProperty("创建人")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;//创建人

    @ApiModelProperty("修改人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;//修改人

}